package com.example.unarea;

import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.app.ListActivity;

public class Bla extends FragmentActivity implements ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	public static boolean x;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bla);
		boolean week = getIntent().getExtras().getBoolean("week");
		x=week;

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shedule, menu);
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 5;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return "��";
			case 1:
				return "��";
			case 2:
				return "��";
			case 3:
				return "��";
			case 4:
				return "��";
								
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		
		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_bla_dummy,
					container, false);
			
			if (x)
			{
			switch(getArguments().getInt(ARG_SECTION_NUMBER))
			{case 1:
			TextView dummyTextView = (TextView) rootView.findViewById(R.id.p1);
			dummyTextView.setText("�������: ����������");
			TextView dummyTextView2 = (TextView) rootView.findViewById(R.id.a1);
			dummyTextView2.setText("���.: 252");
			
			dummyTextView = (TextView) rootView.findViewById(R.id.p2);
			dummyTextView.setText("�������: ������������������");
			dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
			dummyTextView2.setText("���.: 501");
			
			dummyTextView = (TextView) rootView.findViewById(R.id.p3);
			dummyTextView.setText("�������: �����(���)");
			dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
			dummyTextView2.setText("���.: 512/510");
			
			return rootView;
			case 2:
				dummyTextView = (TextView) rootView.findViewById(R.id.p3);
				dummyTextView.setText("�������: �����������/���������������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
				dummyTextView2.setText("���.: 511/506");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p4);
				dummyTextView.setText("�������: ����");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
				dummyTextView2.setText("���.: 506/604");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p5);
				dummyTextView.setText("�������: �������������(���)");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a5);
				dummyTextView2.setText("���.: 511/506");
				return rootView;
			case 3:
				dummyTextView = (TextView) rootView.findViewById(R.id.p3);
				dummyTextView.setText("�������: ������/������������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
				dummyTextView2.setText("���.: 506/303");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p4);
				dummyTextView.setText("�������: ������������� �����������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
				dummyTextView2.setText("���.: 507");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p5);
				dummyTextView.setText("�������: ������������� ���������(���)");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a5);
				dummyTextView2.setText("���.: 510/604");
				return rootView;
			case 4:
				dummyTextView = (TextView) rootView.findViewById(R.id.p2);
				dummyTextView.setText("�������: ������ �����������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
				dummyTextView2.setText("���.: 501");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p3);
				dummyTextView.setText("�������: �����������/���������������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
				dummyTextView2.setText("���.: 511/510");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p4);
				dummyTextView.setText("�������: �����������/���������������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
				dummyTextView2.setText("���.: 511/510");
				
				return rootView;
			case 5:
								
				dummyTextView = (TextView) rootView.findViewById(R.id.p2);
				dummyTextView.setText("�������: ������������/������(���)");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
				dummyTextView2.setText("���.: 511/512");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p3);
				dummyTextView.setText("�������: ������ ������");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
				dummyTextView2.setText("���.: 502");
				
				dummyTextView = (TextView) rootView.findViewById(R.id.p4);
				dummyTextView.setText("�������: ���������� ����");
				dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
				dummyTextView2.setText("���.: 516");
				return rootView;
			}return rootView;}else
			{switch(getArguments().getInt(ARG_SECTION_NUMBER))
				{case 1:
					TextView dummyTextView = (TextView) rootView.findViewById(R.id.p1);
					dummyTextView.setText("�������: ");
					TextView dummyTextView2 = (TextView) rootView.findViewById(R.id.a1);
					dummyTextView2.setText("���.:");
					
					dummyTextView = (TextView) rootView.findViewById(R.id.p2);
					dummyTextView.setText("�������: ������ ������(��)");
					dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
					dummyTextView2.setText("���.: 303");
					
					dummyTextView = (TextView) rootView.findViewById(R.id.p3);
					dummyTextView.setText("�������: �����(���)");
					dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
					dummyTextView2.setText("���.: 511/604");
					
					dummyTextView = (TextView) rootView.findViewById(R.id.p4);
					dummyTextView.setText("�������: ����");
					dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
					dummyTextView2.setText("���.: 507");
						return rootView;
					case 2:
						dummyTextView = (TextView) rootView.findViewById(R.id.p1);
						dummyTextView.setText("�������: ������������� �����������(���)/.");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a1);
						dummyTextView2.setText("���.: 512/.");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p2);
						dummyTextView.setText("�������: ������������������(���)");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
						dummyTextView2.setText("���.: 501/303");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p3);
						dummyTextView.setText("�������: �����");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
						dummyTextView2.setText("���.: 507");
						return rootView;
					case 3:
						dummyTextView = (TextView) rootView.findViewById(R.id.p3);
						dummyTextView.setText("�������: �����������/���������������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
						dummyTextView2.setText("���.: 511/506");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p4);
						dummyTextView.setText("�������: �����������/���������������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a4);
						dummyTextView2.setText("���.: 511/506");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p5);
						dummyTextView.setText("�������: ���������� ����");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a5);
						dummyTextView2.setText("���.: 522");
						return rootView;
					case 4:
						dummyTextView = (TextView) rootView.findViewById(R.id.p1);
						dummyTextView.setText("�������: �����������/���������������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a1);
						dummyTextView2.setText("���.: 511/506");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p2);
						dummyTextView.setText("�������: ������������� ���������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
						dummyTextView2.setText("���.: 501");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p3);
						dummyTextView.setText("�������: �����������/���������������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
						dummyTextView2.setText("���.: 511/510");
						return rootView;
					case 5:
						dummyTextView = (TextView) rootView.findViewById(R.id.p1);
						dummyTextView.setText("�������: ����������(��)");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a1);
						dummyTextView2.setText("���.: 522");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p2);
						dummyTextView.setText("�������: ������ �����������(���)");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a2);
						dummyTextView2.setText("���.: 510/303");
						
						dummyTextView = (TextView) rootView.findViewById(R.id.p3);
						dummyTextView.setText("�������: ������ �����������");
						dummyTextView2 = (TextView) rootView.findViewById(R.id.a3);
						dummyTextView2.setText("���.: 502");
						return rootView;
					}return rootView;}
		}			
			
			
			
		}
	}


