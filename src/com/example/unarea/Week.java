package com.example.unarea;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

import android.widget.RadioButton;


public class Week extends Activity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_week);
		final RadioButton rb = (RadioButton)findViewById(R.id.radio1);
		findViewById(R.id.okbtn).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						switch (view.getId()) {
					    case R.id.okbtn:
					    	Intent intent = new Intent(Week.this, Bla.class);
					    if (rb.isChecked())
					    	{intent.putExtra("week", true);}
					    	else{intent.putExtra("week", false);}
					    	startActivity(intent);
					      break;
					    default:
					      break;
					}}});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.week, menu);
		return true;
	}

}
